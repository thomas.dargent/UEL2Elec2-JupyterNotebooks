# UEL2Elec2-JupyterNotebooks

Lien vers le notebook de Rappels Python: [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gh/tdulille/UEL2Elec2-JupyterNotebooks/main?filepath=Revision-Cours-MichelFryziel.ipynb)  

Lien vers un exemple de compte rendu de TP sous Jupyter Notebook: [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gh/tdulille/UEL2Elec2-JupyterNotebooks/main?filepath=Exemple%20de%20compte%20rendu%20de%20TP.ipynb)


Lien vers le notebook du TD 1 (2021-2022) [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.univ-lille.fr%2Fthomas.dargent%2FUEL2Elec2-JupyterNotebooks/main?urlpath=-%2Fblob%2Fmain%2Fseance1-sansCorrection.ipynb)